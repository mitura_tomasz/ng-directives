import { Directive, ElementRef, OnInit, AfterViewInit ,ViewChild, ComponentRef, ComponentFactoryResolver, ViewContainerRef, Input } from '@angular/core';
import { TooltipComponent } from '../components/tooltip/tooltip.component';

@Directive({
  selector: '[appTooltip]',
  host: {
    '(mouseenter)': 'showtooltip()',
    '(mouseleave)': 'hidetooltip()'
  }
})
export class TooltipDirective {

  @Input() appTooltip: string = '';
  private element: HTMLElement;
  private tooltipRef: ComponentRef<TooltipComponent>;
  
  constructor(
    private _elementRef: ElementRef,
    private viewContainer: ViewContainerRef,
    private _componentFactoryResolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {
    this.element = this._elementRef.nativeElement;
  }
  

  showtooltip() {
    this.tooltipRef = this.createtooltip(this.appTooltip);
    let tooltipElem = this.tooltipRef.location.nativeElement;
    let targetPos = this.getTargettooltipLocation();

    tooltipElem.style.left = targetPos.x + 'px';
    tooltipElem.style.top = targetPos.y + 'px';
  }

  hidetooltip() {
    if (this.tooltipRef) {
      this.tooltipRef.destroy();
      this.tooltipRef = null;
    }
  }

  createtooltip(message: string): ComponentRef<TooltipComponent> {
    this.viewContainer.clear();

    let tooltipComponentFactory = this._componentFactoryResolver.resolveComponentFactory(TooltipComponent);
    let tooltipComponentRef = this.viewContainer.createComponent(tooltipComponentFactory);

    tooltipComponentRef.instance.message = message;

    return tooltipComponentRef;
  }
  
  getTargettooltipLocation(): Point {
    let box = this.element.getBoundingClientRect();
    return new Point((box.width / 2) * -1 , 3);
  }

}

export class Point {
  constructor(public x: number, public y: number) {};
}