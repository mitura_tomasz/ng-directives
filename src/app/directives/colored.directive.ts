import { Input,
  Directive,
  HostListener,
  ElementRef 
} from '@angular/core';

@Directive({
  selector: '[appColored]',
  host: {
    '(mouseover)': 'color()',
    '(mouseleave)': 'decolor()',
  }
})
export class ColoredDirective {

  constructor(private _elementRef: ElementRef) {}
    
  color() {
    this._elementRef.nativeElement.style.color = 'red';
  }
  decolor() {
    this._elementRef.nativeElement.style.color = 'black';
  }
  
}
