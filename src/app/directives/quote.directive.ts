import { Directive, Input, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appQuote]',
  host: {
    '[class]': '"quote-text"'
  }
})
export class QuoteDirective {
  
  constructor(private _elementRef: ElementRef) {}
  
  @Input() politicianAuthor: string;
  @Input() politicianQuote: string;
  
  ngOnInit() {
    this._elementRef.nativeElement.innerHTML =
    `
      <i>"${this.politicianQuote}"</i><br>
      - ${this.politicianAuthor}
    `;
  }

}
