import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appNumbersOnly]'
})
export class NumbersOnlyDirective {
 
  private regex: RegExp = new RegExp(/^[0-9]*$/g); // Allow decimal numbers.
  private specialKeys: Array<string> = [ 'Backspace', 'Tab', 'End', 'Home', 'Delete', 'ArrowLeft', 'ArrowRight' ]; // Allowed special keys

  constructor(private _elementRef: ElementRef) { }

  @HostListener('keydown', [ '$event' ])
  onKeyDown(event: KeyboardEvent) {
    
    if (this.specialKeys.indexOf(event.key) !== -1) {
        return;
    }
    
    let current: string = this._elementRef.nativeElement.value;
    
    let next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {
        event.preventDefault();
    }
  }
}