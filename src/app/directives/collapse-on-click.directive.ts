import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appCollapseOnClick]'
})
export class CollapseOnClickDirective {

  isCollapsed = false;

  @HostBinding('class.collapsed') get collapsed() {
    return this.isCollapsed;
  }


  constructor() { }

  @HostListener('click') toggle(): void {
    console.log('Kliknieto');
    this.isCollapsed = !this.isCollapsed;
  }

}
