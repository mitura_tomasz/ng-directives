import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NumbersOnlyDirective } from './directives/numbers-only.directive';
import { ColoredDirective } from './directives/colored.directive';
import { TooltipDirective } from './directives/tooltip.directive';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { CollapseOnClickDirective } from './directives/collapse-on-click.directive';
import { QuoteDirective } from './directives/quote.directive';


@NgModule({
  declarations: [
    AppComponent,
    NumbersOnlyDirective,
    ColoredDirective,
    TooltipDirective,
    TooltipComponent,
    CollapseOnClickDirective,
    QuoteDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  entryComponents: [
    TooltipComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
